# prepopulate.py
from sqlalchemy.exc import IntegrityError

from inventory_module import app
from inventory_module.models import InventoryModel, BootOptionsModel, InventoryFormOptionsType, InventoryFormOptionsModel

def seed():
    """
    Populates the database with config information
    """
    with app.app_context():
        try:
            default_options = app.config.get("DEFAULT_BOOT_OPTIONS")

            for key, value in default_options.items():
                BootOptionsModel.create(id=key, name=value)

            try:
                BootOptionsModel.session.commit()
            except IntegrityError as err:
                print("Error seeding the database: ", err)

        except:
            print('Boot options already exist in the database')

        try:
            default_forms = app.config.get("DEFAULT_FORM_TYPES")

            for key, value in default_forms.items():
                InventoryFormOptionsType.create(id=key,
                                  options_type=value['type'],
                                  has_boot_options=value['boot_options'],
                                  has_mac_address=value['mac_address'],
                                  active=True)

            try:
                InventoryFormOptionsType.session.commit()
            except IntegrityError as err:
                print("Error seeding the database: ", err)

        except:
            print('Form options types already exist in the database')


if __name__ == '__main__':
    seed()
