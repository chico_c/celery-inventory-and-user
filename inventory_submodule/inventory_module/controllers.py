###decorators# inventory/controllers.py
import os
from flask import jsonify, abort, make_response, send_from_directory, current_app
from flask_restful import Resource, reqparse
from sqlalchemy.exc import DatabaseError
from werkzeug.datastructures import FileStorage

from .models import (
    BootOptionsModel, InventoryModel, InventoryFormOptionsModel,
    InventoryFormOptionsType, InventorySchema, BootOptionsSchema,
    InventoryFormOptionsSchema, InventoryFormOptionsModelSchema
)
from .utilities import get_dt

from sqlalchemy_pagination import paginate
from . import celery, cel_user


class Inventory(Resource):

    def __init__(self):
        self.inventory_schema = InventorySchema()

        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('tag', type=str)
        parser.add_argument('serial', type=str)
        parser.add_argument('type', type=str)
        parser.add_argument('model', type=str)
        parser.add_argument('description', type=str)
        parser.add_argument('buydate', type=get_dt)
        parser.add_argument('warranty', type=get_dt)
        parser.add_argument('location', type=str)
        parser.add_argument('prev_location', type=str)
        parser.add_argument('active', type=bool)
        parser.add_argument('mac_address', type=str)
        parser.add_argument('boot_option_id', type=int)
        parser.add_argument('file_name', type=str)
        parser.add_argument('original_tag', type=str)
        parser.add_argument('owner_id', type=int, required=True)
        parser.add_argument('original_owner', type=int)
        parser.add_argument('file', type=FileStorage, location='file')
        self.args = parser.parse_args()

        super().__init__()

    def post(self):
        if InventoryModel.find_by_tag(self.args['tag']):
            return abort(
                400,
                'An item with tag#: {tag} already exists'.format(tag=self.args['tag'])
            )

        file = self.args.pop("file")
        # boot_id = self.args.pop("boot_option_id")
        fields_to_create = dict((k, v) for k, v in self.args.items() if v)
        fields_to_create['active'] = self.args['active']

        celery.send_task("inventory.add_item", args=[fields_to_create, file])

        return make_response(
            jsonify(
                message='Item with tag#: {tag} was added '
                        'to the inventory'.format(tag=self.args['tag'])
            ), 201
        )

    def put(self):
        inventory = InventoryModel.find_by_tag(self.args['original_tag'])

        if not inventory:
            return abort(
                404,
                'Item with id: {tag} could not be found. '
                'Item was not updated'.format(tag=self.args['original_tag'])
            )

        elif inventory:

            file = self.args.pop("file")
            #boot_id = self.args.pop("boot_option_id")
            original_tag = self.args.pop("original_tag")
            original_owner = self.args.pop("original_owner")
            fields_to_update = dict((k, v) for k, v in self.args.items() if v)
            fields_to_update['active'] = self.args['active']

            if self.args['location'] != inventory.location:
                fields_to_update['prev_location'] = inventory.location


            celery.send_task("inventory.edit_item", args=[fields_to_update, file, original_tag, original_owner])


            return make_response(
                jsonify(
                    message='Item with tag#: {} was updated in the database.'.format(self.args['tag'])
                ), 202
            )


class GetOneInventory(Resource):

    def __init__(self):
        self.inventory_schema = InventorySchema()
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('tag', type=str)
        self.args = parser.parse_args()
        super().__init__()

    def post(self):
        inventory = InventoryModel.find_by_tag(self.args['tag'])
        inventory = self.inventory_schema.dump(inventory)

        item_ids = [x['id'] for x in inventory]

        owners = cel_user.send_task("users.send_users", args=[item_ids])
        owners = owners.get()

        for x, y in zip(inventory, owners):
            x['owner'] = y

        if inventory:
            return jsonify(inventory)

        return jsonify(message='No item with that tag exists.')


class InventoryDeactivate(Resource):

    def __init__(self):
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('tag', type=str)
        parser.add_argument('active', type=bool)
        self.args = parser.parse_args()
        super().__init__()

    def put(self):
        celery.send_task("inventory.edit_item", args=[self.args])

        return make_response(
            jsonify(
                message='Item with tag#: {tag} was deactivated'.format(tag=self.args["tag"])
            ), 202
        )


class InventoryList(Resource):

    def __init__(self):
        self.inventory_schema = InventorySchema(many=True)
        super().__init__()

    def get(self):
        inventory = self.inventory_schema.dump(InventoryModel.query.all())

        item_ids = [x['id'] for x in inventory]

        owners = cel_user.send_task("users.send_users", args=[item_ids])
        owners = owners.get()

        for x, y in zip(inventory, owners):
            x['owner'] = y

        return jsonify(inventory=inventory,
                       image_dir=current_app.config['INVENTORY_IMAGES_DIR'])


class BootOptionsList(Resource):

    def __init__(self):
        self.boot_schema = BootOptionsSchema(many=True)
        super().__init__()

    def get(self):
        boot_options = BootOptionsModel.query.filter_by(active=True).order_by(BootOptionsModel.id).all()
        return jsonify(boot_options=self.boot_schema.dump(boot_options))


class BootOptions(Resource):

    def __init__(self):
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument("boot_option", type=str)
        parser.add_argument("id", type=int)
        self.args = parser.parse_args()
        super().__init__()

    def post(self):
        if BootOptionsModel.find_by_boot_option(self.args['boot_option']):
            return abort(
                400,
                '{} already exists in the database'.format(self.args['boot_option'])
            )
        else:
            try:
                BootOptionsModel.create(
                        name=self.args['boot_option'],
                        active=True
                )

            except DatabaseError:
                return abort(
                    500,
                    'An error occurred while saving {}'
                    'to the database'.format(self.args['boot_option'])
                )

            return make_response(
                jsonify(
                    message='New boot option added to the database!'
                ), 201
            )

    def put(self):
        boot = BootOptionsModel.query.filter_by(id=(self.args['id'])).first()
        boot.active = False

        try:
            boot.update(name=boot.name, active=boot.active)
        except DatabaseError:
            return abort(
                500,
                'An error occurred while deactivating the boot option '
                'with id \'{boot}\''.format(boot=self.args["id"])
            )
        else:
            return make_response(
                jsonify(
                    message='Boot option with id: {boot} was deactivated'.format(boot=self.args["id"])
                ), 202
            )

    def __del__(self):
        BootOptionsModel.session.commit()


class InventoryFormType(Resource):

    def __init__(self):
        self.inventory_form_schema = InventoryFormOptionsSchema(many=True)
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('options_type', type=str)
        parser.add_argument('has_mac_address', type=bool)
        parser.add_argument('has_boot_options', type=bool)
        self.args = parser.parse_args()
        super().__init__()

    def get(self):
        results = list(map(lambda x: x.options_type, InventoryFormOptionsType.query.filter_by(active=True).all()))

        print(results)

        mac_address_results = InventoryFormOptionsType.query.filter_by(has_mac_address=True).all()

        boot_option_results = InventoryFormOptionsType.query.filter_by(has_boot_options=True).all()

        return make_response(
            jsonify(
                options_type=results,
                mac_address_results=self.inventory_form_schema.dump(mac_address_results),
                boot_option_results=self.inventory_form_schema.dump(boot_option_results)

            ), 200
        )

    def post(self):
        if InventoryFormOptionsType.find_by_options_type(self.args['options_type']):
            return abort(
                400,
                '{} already exists in the database'.format(self.args['options_type'])
            )
        else:
            try:
                InventoryFormOptionsType.create(**self.args)

            except DatabaseError:
                return abort(
                    500,
                    'An error occurred while saving {} type'
                    'to the database'.format(self.args['options_type'])
                )

            return make_response(
                jsonify(
                    message='New type added to the database!'
                ), 201
            )

    def put(self):
        type = InventoryFormOptionsType.find_by_options_type(self.args["options_type"])

        try:
            type.update(options_type=type.options_type, has_mac_address=type.has_mac_address, has_boot_options=type.has_boot_options, active=False)
        except DatabaseError:
            return abort(
                500,
                'An error occurred while deactivating the  '
                'type \'{type}\''.format(type=self.args["options_type"])
            )
        else:
            return make_response(
                jsonify(
                    message='Type: {type} was deactivated'.format(type=self.args["options_type"])
                ), 202
            )

    def __del__(self):
        InventoryFormOptionsType.session.commit()


class InventoryFormModel(Resource):

    def __init__(self):
        self.inventory_form_schema = InventoryFormOptionsModelSchema(many=True)
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('options_model', type=str)
        self.args = parser.parse_args()
        super().__init__()

    def __del__(self):
        InventoryFormOptionsModel.session.commit()

    def get(self):
        inventory_form_options = InventoryFormOptionsModel.query.filter_by(active=True)

        if inventory_form_options.count() == 0:
            return make_response(
                jsonify(
                    options_model=inventory_form_options.all()
                ), 200
            )

        return make_response(
            jsonify(
                options_model=self.inventory_form_schema.dump(
                    inventory_form_options.all()
                )
            ), 200
        )

    def post(self):
        form_options = InventoryFormOptionsModel.find_by_options_model(
            self.args['options_model']
        )
        if form_options:
            return abort(
                400,
                'Model: {} already exists in the database'.format(
                    self.args['options_model']
                )
            )
        else:
            try:
                InventoryFormOptionsModel.create(**self.args)
            except DatabaseError:
                return abort(
                    500,
                    'An error occurred while saving {} model'
                    ' to the database'.format(self.args['options_model'])
                )
            else:
                return make_response(
                    jsonify(
                        message='New model added to the database'
                    ), 201
                )

    def put(self):
        model = InventoryFormOptionsModel.find_by_options_model(self.args["options_model"])

        try:
            model.update(options_model=model.options_model, active=False)
        except DatabaseError:
            return abort(
                500,
                'An error occurred while deactivating the inventory form'
                'with options number \'{model}\''.format(model=self.args["options_model"])
            )
        else:
            return make_response(
                jsonify(
                    message='Option with #: {model} was deactivated'.format(model=self.args["options_model"])
                ), 202
            )


class InventoryFormModelSearch(Resource):

    def __init__(self):
        self.inventory_schema = InventorySchema(many=True)
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('model', type=str)
        self.args = parser.parse_args()
        super().__init__()

    def get(self):
        search_result = InventoryFormOptionsModel.search_options_model(self.args['model'])

        if search_result:
            return jsonify(results=self.inventory_schema.dump(search_result))
        return abort(404, 'No match found')


class InventoryListWithPagination(Resource):

    def __init__(self):
        self.inventory_schema = InventorySchema(many=True)
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('page', type=int, default=1)
        parser.add_argument('per_page', type=int, default=10)
        parser.add_argument('search_string', type=str)
        parser.add_argument('sort_by', type=str)
        self.args = parser.parse_args()
        super().__init__()

    def post(self):
        if self.args['search_string']:
            inventory_query = InventoryModel.search_inventory(self.args['search_string'])
        else:
            inventory_query = InventoryModel.query

        if self.args['sort_by']:
            sort_params = InventoryModel.process_sort_query(self.args['sort_by'])
            inventory_query = inventory_query.order_by(*sort_params)

        inventory = inventory_query
        page = paginate(inventory, self.args['page'], self.args['per_page'])

        inventory = self.inventory_schema.dump(page.items)

        item_ids = [x['id'] for x in inventory]

        owners = cel_user.send_task("users.send_users", args=[item_ids])
        owners = owners.get()

        for x, y in zip(inventory, owners):
            x['owner'] = y


        return jsonify(
            inventory_count=inventory.count(),
            inventory=inventory,
            image_dir=current_app.config['INVENTORY_IMAGES_DIR']
        )


class InventoryActiveWithPagination(Resource):

    def __init__(self):
        self.inventory_schema = InventorySchema(many=True)
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('page', type=int, default=1)
        parser.add_argument('per_page', type=int, default=10)
        parser.add_argument('search_string', type=str, default='')
        parser.add_argument('sort_by', type=str)
        self.args = parser.parse_args()
        super().__init__()

    def post(self):
        if self.args['search_string']:
            inventory_query = InventoryModel.search_inventory(
                self.args['search_string']
            ).filter(InventoryModel.active == True)
        else:
            inventory_query = InventoryModel.query.filter(InventoryModel.active == True)

        if self.args['sort_by']:
            sort_params = InventoryModel.process_sort_query(self.args['sort_by'])
            inventory_query = inventory_query.order_by(*sort_params)

        inventory = inventory_query

        if inventory.count() == 0:
            return jsonify(
                inventory_count=inventory.count(),
                inventory=inventory.all(),
                image_dir=current_app.config['INVENTORY_IMAGES_DIR']
            )

        page = paginate(inventory, self.args['page'], self.args['per_page']).items

        inventory = self.inventory_schema.dump(page.items)

        item_ids = [x['id'] for x in inventory]

        owners = cel_user.send_task("users.send_users", args=[item_ids])
        owners = owners.get()

        for x, y in zip(inventory, owners):
            x['owner'] = y

        return jsonify(
            inventory_count=inventory.count(),
            inventory=inventory,
            image_dir=current_app.config['INVENTORY_IMAGES_DIR']
        )


class InventoryInactiveWithPagination(Resource):

    def __init__(self):
        self.inventory_schema = InventorySchema(many=True)
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('page', type=int, default=1)
        parser.add_argument('per_page', type=int, default=10)
        parser.add_argument('search_string', type=str, default='')
        parser.add_argument('sort_by', type=str)
        self.args = parser.parse_args()
        super().__init__()

    def post(self):
        if self.args['search_string']:
            inventory_query = InventoryModel.search_inventory(
                self.args['search_string']
            ).filter(InventoryModel.active == False)
        else:
            inventory_query = InventoryModel.query.filter(InventoryModel.active == False)

        if self.args['sort_by']:
            sort_params = InventoryModel.process_sort_query(self.args['sort_by'])
            inventory_query = inventory_query.order_by(*sort_params)

        inventory = inventory_query

        page = paginate(inventory, self.args['page'], self.args['per_page'])

        inventory = self.inventory_schema.dump(page.items)

        item_ids = [x['id'] for x in inventory]

        owners = cel_user.send_task("users.send_users", args=[item_ids])
        owners = owners.get()

        for x, y in zip(inventory, owners):
            x['owner'] = y

        return jsonify(
            inventory_count=inventory.count(),
            inventory=inventory
        )


class InventorySearchList(Resource):

    def __init__(self):
        self.inventory_schema = InventorySchema(many=True)
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('params', type=str)
        self.args = parser.parse_args()
        super().__init__()

    def post(self):
        search_result = InventoryModel.search_inventory(self.args['params']).all()

        inventory = self.inventory_schema.dump(search_results)

        item_ids = [x['id'] for x in inventory]

        owners = cel_user.send_task("users.send_users", args=[item_ids])
        owners = owners.get()

        for x, y in zip(inventory, owners):
            x['owner'] = y

        if search_result:
            return jsonify(results=inventory)
        return abort('No match found')


class QueryInventoryByTag(Resource):

    def __init__(self):
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('tag', type=str)
        self.args = parser.parse_args()
        super().__init__()

    def post(self):
        inventory = InventoryModel.find_by_tag(self.args['tag'])

        if inventory:
            return jsonify(message='Item found.')
        return abort(404, 'No item match found.')


class InventoryAdvanceSearch(Resource):

    def get(self):
        pass


class InventoryItemCount(Resource):

    def get(self):
        return jsonify(item_count=InventoryModel.query.count())


class InventoryImage(Resource):

    def __init__(self):
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('tag', type=str)
        self.args = parser.parse_args()
        super().__init__()

    def get(self):
        inventory = InventoryModel.find_by_tag(self.args['tag'])

        if inventory:
            try:
                return send_from_directory(current_app.config['INVENTORY_IMAGES_DIR'], inventory.file_name)
            except FileNotFoundError:
                return abort(404, 'File not found.')
        return abort(404, 'Inventory item not found')
