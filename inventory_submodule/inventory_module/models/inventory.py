# inventory/models/inventory.py
from sqlalchemy import Column, Integer, String, Boolean, DATE, func, Text
from sqlalchemy.sql import or_

from extensions import BaseModel


class InventoryModel(BaseModel):
    __tablename__ = 'inventory'
    __repr_attr__ = ['tag']

    id = Column(Integer, primary_key=True)
    tag = Column('tag', String(80), unique=True, nullable=False)
    serial = Column('serial', String(80), nullable=False)
    type = Column('type', String(80), nullable=False)
    model = Column('model', String(80), nullable=False)
    description = Column('description', String(80), nullable=False)
    buydate = Column('buydate', DATE, default=func.current_date())
    warranty = Column('warranty', DATE)
    location = Column('location', String(80))
    prev_location = Column('prev_location', String(80))
    file_name = Column('file_name', String(96))
    image_path = Column('image_path', Text)
    active = Column('active', Boolean(), default=True)
    owner_id = Column('owner_id', Integer)

    @classmethod
    def find_by_tag(cls, tag):
        return cls.query.filter_by(tag=tag).first()

    @classmethod
    def search_inventory(cls, params):
        param = '%' + params + '%'

        return cls.query.filter(
            or_(
                cls.tag.like(param),
                cls.serial.like(param),
                cls.type.like(param),
                cls.model.like(param),
                cls.description.like(param),
                cls.buydate.like(param),
                cls.warranty.like(param),
                cls.location.like(param),
                cls.prev_location.like(param),
                cls.active.like(param),
            )
        )

    @classmethod
    def process_filter_query(cls, search_string):
        filter_queries = [
            cls.tag.contains(search_string) |
            cls.serial.contains(search_string) |
            cls.type.contains(search_string) |
            cls.model.contains(search_string) |
            cls.description.contains(search_string) |
            cls.buydate.contains(search_string) |
            cls.warranty.contains(search_string) |
            cls.location.contains(search_string) |
            cls.prev_location.contains(search_string)
        ]
        return filter_queries

    @classmethod
    def process_sort_query(cls, sort_by):
        sort_query = []
        if sort_by:
            if sort_by == 'tag_asc':
                sort_query.append(cls.tag.asc())
            if sort_by == 'tag_desc':
                sort_query.append(cls.tag.desc())
            if sort_by == 'serial_asc':
                sort_query.append(cls.serial.asc())
            if sort_by == 'serial_desc':
                sort_query.append(cls.serial.desc())
            if sort_by == 'type_asc':
                sort_query.append(cls.type.asc())
            if sort_by == 'type_desc':
                sort_query.append(cls.type.desc())
            if sort_by == 'model_asc':
                sort_query.append(cls.model.asc())
            if sort_by == 'model_desc':
                sort_query.append(cls.model.desc())
            if sort_by == 'buydate_asc':
                sort_query.append(cls.buydate.asc())
            if sort_by == 'buydate_desc':
                sort_query.append(cls.buydate.desc())
            if sort_by == 'warranty_asc':
                sort_query.append(cls.warranty.asc())
            if sort_by == 'warranty_desc':
                sort_query.append(cls.warranty.desc())
            if sort_by == 'location_asc':
                sort_query.append(cls.location.asc())
            if sort_by == 'location_desc':
                sort_query.append(cls.location.desc())
            if sort_by == 'active_asc':
                sort_query.append(cls.active.asc())
            if sort_by == 'active_desc':
                sort_query.append(cls.active.desc())
        return sort_query
