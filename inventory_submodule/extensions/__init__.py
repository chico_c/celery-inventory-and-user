# modules/extensions/__init__.py
from flask_cors import CORS
from flask_debugtoolbar import DebugToolbarExtension
from flask_mail import Mail
from flask_marshmallow import Marshmallow
from flask_restful import Api
from .database import get_session, init_db
from .base import BaseModel


"""
Handle requests
"""
cors = CORS()

"""
Handle emailing from the server
"""
mail = Mail()


"""
Model serialization
"""
ma = Marshmallow()

"""
Backend API
"""
api = Api()

"""
Help debug routes
"""
route_debugger = DebugToolbarExtension()
