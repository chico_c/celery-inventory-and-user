# modules/models.py
import logging
from flask import abort, redirect, url_for, request
from flask_admin.contrib.sqla import ModelView
from flask_security import current_user
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy_mixins import AllFeaturesMixin
from sqlalchemy.event import listens_for

Base = declarative_base(name="Model")

class BaseModel(Base, AllFeaturesMixin):
    __abstract__ = True

    @classmethod
    def find(cls, p_key):
        if p_key:
            return cls.query.filter(cls.id == int(p_key)).first()
        else:
            return None
