from celery import Celery
from user_module.models import User, OwnedItems, UserSchema, OwnedItemsSchema
from sqlalchemy.exc import DatabaseError
from dateutil.parser import parse as date_parser
from datetime import datetime
import uuid
import os
from flask import current_app
from werkzeug.utils import secure_filename


cel = Celery('users', broker='amqp://localhost//users', backend='amqp://')

@cel.task(name="users.add_user")
def add_user(user):
    try:
        User.create(**user)
        User.session.commit()
    except DatabaseError:
        return 'An error occurred while inserting user with netid \'{net_id}\''.format(net_id=user['net_id'])

    return 'Success'

@cel.task(name="users.edit_user")
def edit_user(user, original_net_id):
    user_found = User.find_user_by_netid(original_net_id)

    try:
        user_found.update(**user)
        User.session.commit()
    except DatabaseError:
        return 'An error occurred while editing user with netid \'{net_id}\''.format(net_id=user['net_id'])

    return 'Success'

@cel.task(name="users.give_ownership")
def give_ownership(user_id, item_id):
    try:
        oi = OwnedItems.create(item_id=item_id)
        user = User.query.filter_by(id=user_id).first()

        user.owned_items.append(oi)

        User.session.commit()
        OwnedItems.session.commit()

    except DatabaseError:
        return 'An error occurred while adding ownership to \'{net_id}\''.format(net_id=user.net_id)

    return 'Success'

@cel.task(name="users.update_ownership")
def update_ownership(user_id, item_id, original_owner):
    try:
        oi = OwnedItems.query.filter_by(item_id=item_id).first()
        original_user = User.query.filter_by(id=original_owner).first()
        new_user = User.query.filter_by(id=user_id).first()

        original_user.owned_items.remove(oi)
        new_user.owned_items.append(oi)

        User.session.commit()
        OwnedItems.session.commit()

    except DatabaseError:
        return 'An error occurred while updating ownership to \'{net_id}\''.format(net_id=new_user.net_id)

    return 'Success'

@cel.task(name="users.send_users")
def send_users(item_ids):
    user_schema = UserSchema(many=True)
    item_owner_schema = OwnedItemsSchema(many=True)

    items = OwnedItems.query.filter(OwnedItems.item_id.in_(item_ids)).all()
    items = item_owner_schema.dump(items)

    user_ids = [x['owner'] for x in items]
    users = User.query.filter(User.id.in_(user_ids)).all()
    users = user_schema.dump(users)

    for x in users:
        for y in users:
            y.pop('password')

    users = [x for x in users]

    return users
