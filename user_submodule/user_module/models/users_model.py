# security/models/users.py
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy import Column, Integer, String, Boolean, ForeignKey
from sqlalchemy.orm import relationship, validates

from extensions import BaseModel

class OwnedItems(BaseModel):
    __tablename__ = 'owned_items'
    id = Column(Integer, primary_key=True)
    item_id = Column(Integer, nullable=False)
    owner_id = Column(Integer, ForeignKey("users.id"))

class User(BaseModel):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    email = Column(String(120), unique=True)
    first_name = Column(String(100), nullable=False)
    last_name = Column(String(100), nullable=False)
    username = Column(String(80), unique=True, nullable=False)
    net_id = Column(String(80), unique=True)
    password = Column(String(255), nullable=False, default='default_password')
    active = Column(Boolean(), default=True)
    is_employee = Column(Boolean(), default=False)
    is_attendee = Column(Boolean(), default=False)
    a_number = Column(String(120), unique=True)
    major = Column(String(120))
    owned_items = relationship("OwnedItems", backref="owner", lazy="dynamic")

    @classmethod
    def find_user_by_username(cls, username):
        return cls.query.filter_by(username=username).first()

    @classmethod
    def find_user_by_netid(cls, net_id):
        return cls.query.filter_by(net_id=net_id).first()

    def is_pathway_mentor(self):
        if self.has_role('_pathway | mentor'):
            return True
        return False

    @hybrid_property
    def name(self):
        return "{last_name}, {first_name}".format(
            first_name=self.first_name,
            last_name=self.last_name
        )

    def __repr__(self):
        return self.username

    @validates('first_name', 'last_name')
    def name_capitalization(self, key, value):
        return value.capitalize()
