# inventory/__init__.py
import os
from flask import Blueprint, Flask
from flask_restful import Api
from extensions import (
    api, cors, mail, route_debugger,
    ma, init_db, get_session, BaseModel
)
from celery import Celery

celery = Celery('user_util', broker='amqp://localhost//users', backend='amqp://')

""" Startup Configuration """
app = Flask(__name__)

"""
Initialize module configuration
Loads the settings for extensions
"""
import config

user_bp = Blueprint('user_bp', __name__)
user_api = Api(user_bp)

app.register_blueprint(user_bp)

""" Register routes for modules """
import user_module.routes

engine, session = get_session(app)
BaseModel.set_session(session)

"""
Configure security for the application
"""

import user_module

init_db(app, engine, session)

api.init_app(app)                               # Manage application routes
cors.init_app(app, supports_credentials=True)   # Handle cross realm traffic
ma.init_app(app)                                # Serialization for app models
mail.init_app(app)                              # Manage mailing server

#route_debugger.init_app(app)                    # Browser based debug information

from .models import User

@app.before_first_request
def create_user():
    # Create the Admin user if not created
    if not User.find(1):
        User.create(
            username='admin',
            email='admin@email.com',
            password='password',
            first_name='Super',
            last_name='Admin',
        )
        User.session.commit()
