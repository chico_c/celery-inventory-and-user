# security/controllers.py
from flask import jsonify, current_app, abort, make_response
from flask_restful import Resource, reqparse

from .models import User, UserSchema
from . import celery


class UsersResource(Resource):
    def __init__(self):
        self.user_schema = UserSchema(many=True)

        parser = reqparse.RequestParser()
        parser.add_argument('email', type=str, required=True)
        parser.add_argument('first_name', type=str, required=True)
        parser.add_argument('last_name', type=str, required=True)
        parser.add_argument('username', type=str, required=True)
        parser.add_argument('net_id', type=str, required=True)
        parser.add_argument('original_net_id', type=str)
        parser.add_argument('password', type=str)
        parser.add_argument('active', type=bool)
        parser.add_argument('is_employee', type=bool)
        parser.add_argument('is_attendee', type=bool)
        parser.add_argument('a_number', type=str, required=True)
        parser.add_argument('major', type=str)
        self.args = parser.parse_args()
        super().__init__()

    def post(self):
        if User.find_user_by_netid(self.args['net_id']):
            return abort(
                400,
                'A User with net id#: {net_id} already exists'.format(net_id=self.args['net_id'])
            )
        if User.query.filter_by(a_number=self.args['a_number']).first():
            return abort(
                400,
                'A User with net A-Number#: {a_number} already exists'.format(a_number=self.args['a_number'])
            )

        self.args.pop("original_net_id")
        fields_to_create = dict((k, v) for k, v in self.args.items() if v)

        celery.send_task("users.add_user", args=[fields_to_create])

        return make_response(
            jsonify(
                message='User with netid#: {net_id} was added '
                        'to the database'.format(net_id=self.args['net_id'])
            ), 201
        )

    def put(self):
        user = User.find_user_by_netid(self.args['original_net_id'])

        if not user:
            return abort(
                404,
                'User with netid: {original_net_id} could not be found. '
                'User was not updated'.format(original_net_id=self.args['original_net_id'])
            )

        elif user:
            #boot_id = self.args.pop("boot_option_id")
            original_net_id = self.args.pop("original_net_id")
            fields_to_update = dict((k, v) for k, v in self.args.items() if v)

            celery.send_task("users.edit_user", args=[fields_to_update, original_net_id])

            return make_response(
                jsonify(
                    message='User with netid#: {} was updated in the database.'.format(original_net_id)
                ), 202
            )


class ActiveUsersList(Resource):
    def get(self):
        user_schema = UserSchema(many=True)

        users = User.query.filter_by(active=True).all()

        return make_response(
            jsonify(
                ActiveUsers=user_schema.dump(users)
            ), 200
        )


class QueryActiveUsersList(Resource):
    def __init__(self):
        self.user_schema = UserSchema(many=True)
        parser = reqparse.RequestParser()
        parser.add_argument('search_string', type=str, required=True)

        self.args = parser.parse_args()
        super().__init__()

    def get(self):
        users = User.query.filter(User.active == True, User.username.contains(self.args['search_string'])).all()
        return make_response(
            jsonify(
                ActiveUsers=user_schema.dump(users)
            ), 200
        )


class SearchByNetIDUser(Resource):
    def __init__(self):
        self.user_schema = UserSchema()
        parser = reqparse.RequestParser()
        parser.add_argument('search_string', type=str)
        self.args = parser.parse_args()
        super().__init__()

    def get(self):
        search_result = User.query.filter(
            User.username == self.args['search_string']
        ).first()

        if search_result:
            return make_response(
                jsonify(
                    user_schema.dump(search_result)
                ), 200
            )
        else:
            return abort(404, message='User not found')
