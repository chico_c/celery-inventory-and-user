# inventory/routes.py
from importlib import import_module

from user_module import app
from . import user_api


with app.app_context():
    api_config = app.config.get("USER_MODULE_ROUTES")
    if api_config:
        user_resources = import_module("user_module.controllers")
        for api_name in api_config.keys():
            if api_config[api_name].get("url") is None:
                continue
            api_resource = getattr(user_resources, api_name)
            user_api.add_resource(
                api_resource, *(api_config[api_name]['url'], api_config[api_name]['url'] + '/')
            )
    else:
        print("Error: Failed to load routes for the user module.")
