from user_module import app

if __name__ == '__main__':
    app.run(
        host='127.0.0.1',
        port=8081,
        use_reloader=True
    )
