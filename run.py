import os


print('Setting up inventory virtual host...')
os.system('start cmd /c "rabbitmq-server start & rabbitmqctl add_vhost /inventory & rabbitmqctl set_permissions -p /inventory guest ".*" ".*" ".*""')

print('Setting up user virtual host...')
os.system('start cmd /c "rabbitmq-server start & rabbitmqctl add_vhost /users & rabbitmqctl set_permissions -p /users guest ".*" ".*" ".*""')

print('Starting Inventory Worker...')
os.system('start cmd /k "cd inventory_submodule & celery -A inventory_tasks.cel -P eventlet worker -n inventory --loglevel=ERROR"')

print('Starting User Worker...')
os.system('start cmd /k "cd user_submodule & celery -A user_tasks.cel -P eventlet worker -n users --loglevel=ERROR"')

print('Starting Submodules...')
os.system('start cmd /k "cd inventory_submodule & python main.py"')
os.system('start cmd /k "cd user_submodule & python main.py"')
